/*
    SPDX-FileCopyrightText: 2009 Jakub Wieczorek <faw217@gmail.com>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

#include "OpenSearchWriter.h"

#include "OpenSearchEngine.h"

#include <QIODevice>

OpenSearchWriter::OpenSearchWriter()
    : QXmlStreamWriter()
{
    setAutoFormatting(true);
}

bool OpenSearchWriter::write(QIODevice *device, OpenSearchEngine *engine)
{
    if (!engine) {
        return false;
    }

    if (!device->isOpen()) {
        device->open(QIODevice::WriteOnly);
    }

    setDevice(device);
    write(engine);
    return true;
}

void OpenSearchWriter::write(OpenSearchEngine *engine)
{
    writeStartDocument();
    writeStartElement(QStringLiteral("OpenSearchDescription"));
    writeDefaultNamespace(QStringLiteral("http://a9.com/-/spec/opensearch/1.1/"));

    if (!engine->name().isEmpty()) {
        writeTextElement(QStringLiteral("ShortName"), engine->name());
    }

    if (!engine->description().isEmpty()) {
        writeTextElement(QStringLiteral("Description"), engine->description());
    }

    if (!engine->searchUrlTemplate().isEmpty()) {
        writeStartElement(QStringLiteral("Url"));
        writeAttribute(QStringLiteral("method"), QStringLiteral("get"));
        writeAttribute(QStringLiteral("template"), engine->searchUrlTemplate());

        if (!engine->searchParameters().empty()) {
            writeNamespace(QStringLiteral("http://a9.com/-/spec/opensearch/extensions/parameters/1.0/"), QStringLiteral("p"));

            QList<OpenSearchEngine::Parameter>::const_iterator end = engine->searchParameters().constEnd();
            QList<OpenSearchEngine::Parameter>::const_iterator i = engine->searchParameters().constBegin();
            for (; i != end; ++i) {
                writeStartElement(QStringLiteral("p:Parameter"));
                writeAttribute(QStringLiteral("name"), i->first);
                writeAttribute(QStringLiteral("value"), i->second);
                writeEndElement();
            }
        }

        writeEndElement();
    }

    if (!engine->suggestionsUrlTemplate().isEmpty()) {
        writeStartElement(QStringLiteral("Url"));
        writeAttribute(QStringLiteral("method"), QStringLiteral("get"));
        writeAttribute(QStringLiteral("type"), QStringLiteral("application/x-suggestions+json"));
        writeAttribute(QStringLiteral("template"), engine->suggestionsUrlTemplate());

        if (!engine->suggestionsParameters().empty()) {
            writeNamespace(QStringLiteral("http://a9.com/-/spec/opensearch/extensions/parameters/1.0/"), QStringLiteral("p"));

            QList<OpenSearchEngine::Parameter>::const_iterator end = engine->suggestionsParameters().constEnd();
            QList<OpenSearchEngine::Parameter>::const_iterator i = engine->suggestionsParameters().constBegin();
            for (; i != end; ++i) {
                writeStartElement(QStringLiteral("p:Parameter"));
                writeAttribute(QStringLiteral("name"), i->first);
                writeAttribute(QStringLiteral("value"), i->second);
                writeEndElement();
            }
        }

        writeEndElement();
    }

    if (!engine->imageUrl().isEmpty()) {
        writeTextElement(QStringLiteral("Image"), engine->imageUrl());
    }

    writeEndElement();
    writeEndDocument();
}

